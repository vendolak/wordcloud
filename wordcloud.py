from ex7 import *
from turtle import *
from random import *

counter = 0
word_count = 0
count_words_list = []
#c = 0
colormode(255)
fontlist = ["Arial", "Comic Sans", "Comic Sans MS", "Times New Roman"]
stylelist = ["normal", "italic", "bold", "underline"]

with open("dontstopmenow.txt", "r") as f:

    def move():

        #sety(0)
        #setheading(0)

        #color("black")
        
        penup()
        forward(100)
        #left(90)
        pendown()
        
    def newln():
    
        penup()
        right(90)
        forward(20)
        #left(90)
        
        setheading(0)
        setx(0)
        
        pendown()

    #print(count_words(f.read()))
    flower = f.read()
    
    count_dict = count_words(flower.lower())
    
    for key in sorted(count_dict, key = count_dict.get, reverse = True):
        #for i in range(10):
        
        count_words_list.append(str(key) + ": " + str(count_dict[key]))
    
    #print(count_words_list)
    
    #print(*count_words_list[:], sep = "\n")
    
    for i in range(len(count_words_list)):
        R = randint(0,255)
        G = randint(0,255)
        B = randint(0,255)
    
        color(R, G, B)
        write(count_words_list[i], align = "left", font = ((choice(fontlist)), 8, (choice(stylelist))))
        move()
        
        if ((i + 1) % 5 == 0):
            newln()
    
    # for j in range(10):
        # count_words_list[j]
    
    # while(c <= 10):
        # for value in count_words_list:
            # print(value)
            # c += 1
            
        #print(count_words_list)

# with open("output.txt", "w") as text_file:
    # for i in range(len(count_words_list)):
        # text_file.write(count_words_list[i] + " ")

# with open("dontstopmenow.txt", "r") as f:

    # #line = f.readline()
    
    # #print(line)
    
    # for lines in f:
        # # for i in range(1, 4):
        # counter += 1
        
        # print(str(counter), ".)", lines)
        
        # words_list = lines.split()
        
        # #print(words_list)
        
        # word_count += len(words_list)
        
        # #count_words_list.append(count_words(lines))

# print("There are", counter, "lines in this file.")
# print("There are", word_count, "words in this file.")
#print(count_words_list)

done()
